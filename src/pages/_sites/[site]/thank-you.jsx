import Head from 'next/head'

import { SimpleLayout } from '../../../components/SimpleLayout'
import { getHostnameDataBySubdomain, getSubdomainPaths } from '../../../lib/db'


export default function ThankYou() {
  return (
    <>
      <Head>
        <title>You’re subscribed - Spencer Sharp</title>
        <meta
          name="description"
          content="Thanks for subscribing to my newsletter."
        />
      </Head>
      <SimpleLayout
        title="Thanks for subscribing."
        intro="I’ll send you an email any time I publish a new blog post, release a new project, or have anything interesting to share that I think you’d want to hear about. You can unsubscribe at any time, no hard feelings."
      />
    </>
  )
}
export async function getStaticPaths() {
  return {
    paths: await getSubdomainPaths(),
    fallback: true, // fallback true allows sites to be generated using ISR
  }
}

export async function getStaticProps({ params: { site } }) {
  return {
    props: await getHostnameDataBySubdomain(site),
    revalidate: 3600, // set revalidate interval of 1 hour
  }
}
